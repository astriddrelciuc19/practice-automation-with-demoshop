package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CheckoutManagementTest {
    Page page = new Page();
    Header header = new Header();

    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }

    @Test
    public void after_adding_one_product_to_cart_user_can_navigate_from_cart_page_to_checkout_page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        assertEquals("Checkout Page", "Checkout Page", "After adding one product to cart, user can navigate to the Checkout Page.");
    }

    @Test
    public void user_can_add_first_name_in_the_checkout_page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("Smith");
        assertEquals("First Name", "First Name", "User can add his first name in the checkout page.");
    }

    @Test
    public void user_can_add_his_last_name_in_the_checkout_page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInLastName("John");
        assertEquals("Last Name", "Last Name", "User can add his last name in the checkout page.");
    }

    @Test
    public void user_can_add_his_address_in_the_address_field_from_checkout_page() {
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInAddress("Street of Liberty, 9A");
        assertEquals("Address", "Address", "User can add his address in the address field from the checkout page.");
    }
    @Description("Guest User can add his information in the Checkout Page and navigate to the next page.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Astrid Drelciuc")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("User can add his address information.")
    @Test
    public void user_can_add_all_his_information_in_the_checkout_page(){
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("Smith");
        modal.typeInLastName("John");
        modal.typeInAddress("Street of Liberty, 9A");
        header.clickOnTheContinueCheckoutButton();
        assertEquals("Address information", "Address information", "User is expected to be able to add all of his information in the checkout page.");
    }
    @Test
    public void user_can_place_an_order_successfully(){
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("Smith");
        modal.typeInLastName("John");
        modal.typeInAddress("Street of Liberty, 9A");
        header.clickOnTheContinueCheckoutButton();
        header.clickOnTheCompleteYourOrderButton();
        assertEquals("Thank you for your order!", "Thank you for your order!", "After pressing on complete your order button, the Thank you for your order! message is displayed!");
    }

    @Description("User Dino can place an order successfully on Demo Shop.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Astrid Drelciuc")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("After pressing on complete your order button, the Thank you for your order! message is displayed! for Dino User.")
    @Test
    public void user_dino_can_place_an_order_successfully(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("Taylor");
        modal.typeInLastName("Elizabeth");
        modal.typeInAddress("Street of Dreams, 2B");
        header.clickOnTheContinueCheckoutButton();
        header.clickOnTheCompleteYourOrderButton();
        assertEquals("Thank you for your order!", "Thank you for your order!", "After pressing on complete your order button, the Thank you for your order! message is displayed!");
    }

    @Description("User Turtle can place an order successfully on Demo Shop.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Astrid Drelciuc")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("After pressing on complete your order button, the Thank you for your order! message is displayed! for Turtle User.")
    @Test
    public void user_turtle_can_place_an_order_successfully(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("1");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("Taylor");
        modal.typeInLastName("Elizabeth");
        modal.typeInAddress("Street of Dreams, 2B");
        header.clickOnTheContinueCheckoutButton();
        header.clickOnTheCompleteYourOrderButton();
        assertEquals("Thank you for your order!", "Thank you for your order!", "After pressing on complete your order button, the Thank you for your order! message is displayed!");
    }

    @Description("User Beetle can place an order successfully on Demo Shop.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Astrid Drelciuc")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("After pressing on complete your order button, the Thank you for your order! message is displayed! for Beetle User.")
    @Test
    public void user_beetle_can_place_an_order_successfully(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("8");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        header.clickOnTheCheckoutButton();
        modal.typeInFirstName("Taylor");
        modal.typeInLastName("Elizabeth");
        modal.typeInAddress("Street of Dreams, 2B");
        header.clickOnTheContinueCheckoutButton();
        header.clickOnTheCompleteYourOrderButton();
        assertEquals("Thank you for your order!", "Thank you for your order!", "After pressing on complete your order button, the Thank you for your order! message is displayed!");
    }
}
