package org.fasttrackit;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class DemoShopCartTest {

    Page page = new Page();
    Header header = new Header();

    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup(){
        page.openHomePage();
    }
    @AfterMethod
    public void cleanup(){
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }
    @Test

    public void user_can_navigate_to_Cart_Page(){
        header.clickOnTheCartIcon();
        assertEquals(page.getPageTitle(), "Your cart", "Expected to be on the Wishlist page.");
    }
    @Test

    public void user_can_navigate_to_Home_page_from_Cart_page(){
        header.clickOnTheCartIcon();
        header.clickOnTheShoppingBagIcon();
        assertEquals(page.getPageTitle(), "Products", "Expected to be on Products page.");
    }

    @Test

    public void user_can_add_product_to_cart_from_product_cards(){
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "1", "After adding one product to cart, badge shows 1.");
    }
    @Test

    public void user_can_add_two_products_to_cart_from_product_cards(){
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "2", "After adding two products to cart, badge shows 2.");
    }
    @Test

    public void user_can_add_three_products_to_cart_from_product_cards(){
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        assertTrue(header.isShoppingBadgeVisible());
        String badgeValue = header.getShoppingCartBadgeValue();
        assertEquals(badgeValue, "3", "After adding three products to cart, badge shows 3.");
    }
}