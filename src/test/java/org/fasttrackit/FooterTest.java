package org.fasttrackit;

import com.beust.ah.A;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FooterTest {

    @BeforeClass
    public void setup(){

        page.openHomePage();
    }
    Page page = new Page();

    @Test
    public void user_can_refresh_the_homepage() {
        Footer footer= new Footer();
        footer.clickOnTheResetButton();
        assertEquals("Home Page", "Home Page", "Expected to be on the Home Page after clicking on the refresh icon.");
    }
    @Test
    public void user_can_click_on_the_help_icon(){
        Footer footer = new Footer();
        footer.clickOnTheHelpIcon();
        assertEquals("Help Icon", "Help Icon", "User can click on the help icon.");
    }
}