package org.fasttrackit;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class SearchProductTest {
    Page page = new Page();
    Header header = new Header();

    ModalDialog modal = new ModalDialog();

    ProductCards productList = new ProductCards();

    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
        header.clickOnTheShoppingBagIcon();
    }
    @Test
    public void user_can_search_products_that_contains_the_word_practical_by_typing_practical(){
        header.clickOnTheSearchBar();
        modal.typeInWord("practical");
        header.clickOnTheSearchButton();
        assertEquals("Practical", "Practical", "All the products containing the word practical are displayed.");
    }
    @Test
    public void user_can_search_products_that_contains_the_word_awesome_by_typing_awesome(){
        header.clickOnTheSearchBar();
        modal.typeInWord("awesome");
        header.clickOnTheSearchButton();
        assertEquals("Awesome", "Awesome", "All the products containing the word awesome are displayed.");
    }
    @Test
    public void user_can_search_one_particular_product_by_typing_the_product_entire_name(){
        header.clickOnTheSearchBar();
        modal.typeInWord("Incredible Concrete Hat");
        header.clickOnTheSearchButton();
        assertEquals("Incredible Concrete Hat", "Incredible Concrete Hat", "When searching by using the name Incredible Concrete Hat, one specific product is expected to be displayed");
    }
}
