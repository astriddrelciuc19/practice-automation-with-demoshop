package org.fasttrackit;

import io.qameta.allure.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class LogoutTest {
    Page page = new Page();
    Header header = new Header();

    ModalDialog modal = new ModalDialog();

    @BeforeClass
    public void setup(){
        page.openHomePage();
    }
    @AfterMethod
    public void cleanup(){
        Footer footer = new Footer();
        footer.clickOnTheResetButton();
    }
    @Description("User dino can log out from the account.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Astrid Drelciuc")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("Logout with valid credentials.")

    @Test
    public void user_dino_can_logout_from_the_account(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(),"Hello guest!", "Logged out with user dino, expected greetings message to be Hello guest!");
    }

    @Description("User turtle can log out from the account.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Astrid Drelciuc")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("Logout with valid credentials.")
    @Test
    public void user_turtle_can_logout_from_the_account(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(),"Hello guest!", "Logged out with user turtle, expected greetings message to be Hello guest!");
    }

    @Description("User beetle can log out from the account.")
    @Severity(SeverityLevel.CRITICAL)
    @Owner("Astrid Drelciuc")
    @Link(name = "FastTrackIt Demo Shop", url = "https://fasttrackit-test.netlify.app/#/")
    @Story("Logout with valid credentials.")

    @Test
    public void user_beetle_can_logout_from_the_account(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        header.clickOnTheLogoutButton();
        assertEquals(header.getGreetingsMessage(),"Hello guest!", "Logged out with user beetle, expected greetings message to be Hello guest!");
    }

}
