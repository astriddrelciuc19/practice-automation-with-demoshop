package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class UsersandProductsInteractionTest {
    Page page = new Page();
    Header header = new Header();
    ModalDialog modal = new ModalDialog();
    CartPage cartPage = new CartPage();


    @BeforeClass
    public void setup() {
        page.openHomePage();
    }

    @AfterMethod
    public void cleanup() {
        Footer footer = new Footer();
        Selenide.refresh();
        Selenide.clearBrowserCookies();
        footer.clickOnTheResetButton();
    }
    @Test
    public void logged_in_user_dino_can_add_a_product_to_cart() {
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }
    @Test
    public void logged_in_user_turtle_can_add_a_product_to_cart(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("5");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }
    @Test
    public void logged_in_user_beetle_can_add_a_product_to_cart(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("8");
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }
    @Test
    public void logged_in_user_dino_can_add_multiple_products_to_cart(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("9");
        product.clickOnTheProductCartIcon();
        product.clickOnTheProductCartIcon();
        header.clickOnTheCartIcon();
        assertFalse(cartPage.isEmptyCartMessageDisplayed());
    }
    @Test
    public void logged_in_user_dino_can_add_a_product_to_wishlist_page(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("dino");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("8");
        header.clickOnTheWishlistIcon();
        assertEquals("Wishlist", "Wishlist", "After clicking on the wishlist icon, one products is expected to be at the wishlist page");
    }
    @Test
    public void logged_in_user_turtle_can_add_a_product_to_wishlist_page(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("turtle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("2");
        header.clickOnTheWishlistIcon();
        assertEquals("Wishlist", "Wishlist", "After clicking on the wishlist icon, one products is expected to be at the wishlist page");
    }
    @Test
    public void logged_in_user_beetle_can_add_a_product_to_wishlist_page(){
        header.clickOnTheLoginButton();
        modal.typeInUsername("beetle");
        modal.typeInPassword("choochoo");
        modal.clickOnTheLoginButton();
        Product product = new Product("2");
        header.clickOnTheWishlistIcon();
        assertEquals("Wishlist", "Wishlist", "After clicking on the wishlist icon, one products is expected to be at the wishlist page");
    }
}
