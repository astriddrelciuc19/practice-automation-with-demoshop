package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ModalDialog {

    private final SelenideElement userName = $("#user-name");
    private final SelenideElement password = $("#password");

    private final SelenideElement loginButton = $(".modal-dialog .fa-sign-in-alt");

    private final SelenideElement errorElement = $(".error");

    private final SelenideElement close = $(".close");

    private final SelenideElement searchBar = $(".form-control");
    private final SelenideElement firstName= $("#first-name");
    private final SelenideElement lastName= $("#last-name");
    private final SelenideElement addressField = $("#address");

    public void typeInUsername(String user) {
        System.out.println("Click on the Username field.");
        userName.click();
        System.out.println("Type in " + user);
        userName.type(user);
    }
    public void typeInPassword ( String pass){
        System.out.println( "Click on the Password field.");
        password.click();
        System.out.println("Type in " + pass);
        password.type(pass);

    }
    public void typeInFirstName (String first){
        System.out.println("Click on the First Name field.");
        firstName.click();
        System.out.println("Type in: " + first);
        firstName.type(first);
    }
    public void typeInLastName(String last){
        System.out.println("Click on the Last Name field.");
        lastName.click();
        System.out.println("Type in: " + last);
        lastName.type(last);
    }
    public void typeInAddress(String address){
        System.out.println("Click on the Address field.");
        addressField.click();
        System.out.println("Type in: " + address);
        addressField.type(address);
    }
    public void clickOnTheLoginButton(){
        System.out.println("Click on the Login button.");
        loginButton.click();
    }
    public void typeInWord(String word){
        System.out.println("Type in: " + word);
        searchBar.type(word);
    }
    public boolean isErrorMsgDisplayed() {
        return this.errorElement.isDisplayed();
    }

    public String getErrorMsg() {
        return this.errorElement.text();
    }

    public void close() {
        this.close.click();
    }

}
