package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement logoutButton = $(".navbar .fa-sign-out-alt");

    private final SelenideElement greetingsElement = $(".navbar-text span span");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement shoppingCartBadge = $(".shopping_cart_badge");
    private final ElementsCollection shoppingCartBadges = $$(".shopping_cart_badge");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");

    private final SelenideElement searchBar = $(" .form-control");
    private final SelenideElement searchButton= $(" .btn-light");

    private final SelenideElement checkoutButton= $(" .btn-success");

    private final SelenideElement continueCheckoutButton= $(" .btn-success");
    private final SelenideElement completeYourOrderButton= $(" .btn-success");

    private final SelenideElement continueShoppingButton= $(" .btn-danger");


    public void clickOnTheLoginButton() {
        loginButton.click();
        System.out.println("Click on the Login button.");
    }

    public String getGreetingsMessage() {
        return greetingsElement.text();

    }
    public void clickOnTheCheckoutButton(){
        System.out.println("click on the checkout button");
        checkoutButton.click();
    }
    public void clickOnTheContinueCheckoutButton(){
        System.out.println("Click on the Continue checkout button");
        continueCheckoutButton.click();
    }
    public void clickOnTheCompleteYourOrderButton(){
        System.out.println("Click on the Complete Your Order Button");
        completeYourOrderButton.click();
    }

    public void clickOnTheWishlistIcon() {
        System.out.println("Click on the Wishlist button.");
        wishlistButton.click();
    }
    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping bag icon.");
        homePageButton.click();
    }
    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart Icon");
        cartIcon.click();
    }
    public String getShoppingCartBadgeValue() {

        return this.shoppingCartBadge.text();
    }
    public boolean isShoppingBadgeVisible() {

        return !this.shoppingCartBadges.isEmpty();
    }
    public void clickOnTheLogoutButton() {
        logoutButton.click();
        System.out.println("click on the Logout Button");
    }
    public void clickOnTheSearchBar(){
        searchBar.click();
        System.out.println("click on the Search Bar");
    }
    public void clickOnTheSearchButton(){
        searchButton.click();
        System.out.println("click on Search Button");
    }
    public void clickOnTheContinueShoppingButton(){
        continueShoppingButton.click();
        System.out.println("click on Continue Shopping Button");
    }

}
